//////////////////////////////////////////////////////////////////////////////
// FILE   : SpainMaps.tol
// PURPOSE: Package holding the maps for the region of spain.
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
NameBlock SpainMaps =
//////////////////////////////////////////////////////////////////////////////
[[
//read only autodoc
Text _.autodoc.name = "SpainMaps";
Text _.autodoc.brief =
"Maps for the regions of spain.";
Text _.autodoc.description =
"This package contains the maps for the regions of spain. The geometric description is stored in shapefiles. The main purpose of this package is provide access to the shapefiles for the different regions of Spain";
Text _.autodoc.url = 
"http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
Set _.autodoc.keys = [[ "Spatial", "Maps", "Spain" ]];
Set _.autodoc.authors = [[ "josp@tol-project.org" ]];
Text _.autodoc.minTolVersion = "v2.0.1 b.1";
Real _.autodoc.version.high = 1;
Real _.autodoc.version.low = 0;
Set _.autodoc.dependencies = Copy(Empty);
Set _.autodoc.nonTolResources = [[
  Text "maps"
]] ;
Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));

Text _.maps_dir = "";
Set _.regionLevels = [[ "comm", "prov", "mun" ]];
Set _.regionDesc =
[[
  Set comm = {[[
    Text name = "Comunidades",
    Text singular = "Comunidad",
    Text file = "spain_regions_ind",
    Text idfield = "COM",
    Text namefield = "NOMBRE99"
  ]]},
  Set prov = {[[
    Text name = "Provincias",
    Text singular = "Provincia",
    Text file = "spain_provinces_ind_2",
    Text idfield = "PROV",
    Text namefield = "NOMBRE99"
  ]]},
  Set mun = {[[
    Text name = "Municipios",
    Text singular = "Municipio",
    Text file = "esp_muni_00",
    Text idfield = "PROVMUN",
    Text namefield = "MUNICIPIO0"
  ]]}
]];

Text GetMapsDirectory( Real void )
{
  Text _.maps_dir
};

Set GetRegionLevelsID( Real void )
{
  Set _.regionLevels
};

Set GetRegionLevelInfo( Text levelID )
{
  Set _.regionDesc[ levelID ]
};

Real _is_started = False;
////////////////////////////////////////////////////////////////////////////
Real StartActions(Real void)
////////////////////////////////////////////////////////////////////////////
{
  If(_is_started, False, {
      Real _is_started := True;
      Text cwd = GetAbsolutePath( "." ) + "/";
      Text _.maps_dir := cwd + "maps/";
      True
    })
}

]];
